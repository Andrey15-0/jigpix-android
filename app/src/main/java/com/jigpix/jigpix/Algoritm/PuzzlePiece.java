package com.jigpix.jigpix.Algoritm;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import java.util.ArrayList;

/**
 * Created by andrey_sereda on 01.09.2016.
 */
public class PuzzlePiece {
    //MARK: - Struct PixelData
    class PixelData {
        int a = 0;
        int r = 0;
        int g = 0;
        int b = 0;
        int avg = 0;
    }

    static int CHOPSIZE = 30;
    private static int TILEPIXELS = CHOPSIZE * CHOPSIZE;

    public Bitmap img = null;
    private Bitmap[] colorImg = new Bitmap[4];
    ArrayList<PixelData[][]> colorImgPixData = new ArrayList<PixelData[][]>();

    private double[] averageBrightness = new double[4];		// clock-wise  1:top-left, 2:top-right, 3:bottom-right, 4:bottom-left
    public int rotation = 0;					//0 1 2 3 clockwise;

    private int bri = 0;  /* average: (red+gre+blu)/3                         */
    private int gre = 0;  /* average green                                    */
    private int rmb = 0;  /* average red - average blue                       */
    int rit = 0;  /* increase in (r+g+b)/3 center to mid right  edge  */
    int bot = 0;  /* increase in (r+g+b)/3 center to mid bottom edge  */

    private int red = 0;
    private int blu = 0;

    public int id = 0;	// my ORIGINAL id.

    private double distanceFromCenter = 0.0;

    public String toString() {
        return String.format("SN=%1$d BRI=%2$d GRE=%3$d RMB=%4$d RIT=%5$d BOT=%6$d", id, bri, gre, rmb, rit, bot);
    }

    private void processTile(PixelData[][] pixels, PuzzlePiece tptr)
    {
        int red = 0, gre = 0, blu = 0, bri = 0;         /* vals per pixel 0-255 */
        int redsum = 0, gresum = 0, blusum = 0, brisum = 0;      /* sums of 15x15 pixels */
        int rit75 = 0, lef75 = 0, top75 = 0, bot75 = 0;       /* sums of 75 bri's     */
        int redavg = 0, greavg = 0, bluavg = 0, briavg = 0;

        /* array is 1-D, let's pretend its 2-D for the moment */
        for (int y=0; y<CHOPSIZE; y++)
        {
            for (int x=0; x<CHOPSIZE; x++)
            {
                /* note----> ++ sequences thru R,G,B, R,G,B values */
                red = pixels[y][x].r;        redsum += red;
                gre = pixels[y][x].g;        gresum += gre;
                blu = pixels[y][x].b;        blusum += blu;
                bri = red+gre+blu;           brisum += bri; /* 3x */

                /* sums of left & right & top & bot thirds */
                if(x < 5){lef75 += bri;}
                if(x > 9){rit75 += bri;}
                if(y < 5){top75 += bri;}
                if(y > 9){bot75 += bri;}
            }
        }

        redavg = redsum/TILEPIXELS;
        greavg = gresum/TILEPIXELS;
        bluavg = blusum/TILEPIXELS;
        briavg = brisum/(TILEPIXELS * 3);       /* was 3x */

        /* load results into TILE struct */
        tptr.bri = briavg;
        tptr.gre = greavg;
        tptr.rmb = redavg - bluavg;
        tptr.rit = ((rit75 - lef75)*7)/10;       /* WARNING: specific */
        tptr.bot = ((bot75 - top75)*7)/10;       /* for 15 x 15 tiles */
        tptr.red = redavg;
        tptr.blu = bluavg;


        //print(" tile info %i %i %i %i %i \n",  tptr.bri, tptr.gre,  tptr.rmb, tptr.rit, tptr.bot);

        /* note on (( )*7)/10 The centers of left and right sample areas are
         * 10 pixels apart; the right edge pixel is only 7 pixels right of
         * center, thus the factor 7/10. Likewise for top and bottom samples
         */

        //print("%i %i %i %i %i \n", tptr.bri, tptr.gre, tptr.rmb, tptr.rit, tptr.bot);
    }

    private PixelData[][] getPixels(Bitmap image)
    {
        int pixelsWide = image.getWidth();
        int pixelsHigh = image.getHeight();

        PixelData[][] pic = new PixelData[pixelsHigh][pixelsWide];

        for (int x=0; x<pixelsWide; x++) {
            for (int y = 0; y < pixelsHigh; y++) {
                int colour = image.getPixel(x, y);

                int red = Color.red(colour);
                int blue = Color.blue(colour);
                int green = Color.green(colour);
                int alpha = Color.alpha(colour);

                int avg = (red + green + blue) / 3;

                pic[y][x] = new PixelData();
                pic[y][x].avg = avg;
                pic[y][x].a = alpha;
                pic[y][x].r = red;
                pic[y][x].g = green;
                pic[y][x].b = blue;
            }
        }

        return pic;
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    void calculateStatistics(Bitmap imagePiece, int cols, int rows)
    {
        this.rotation = 0;

        for (int i=0; i<4; i++)
        {
            this.averageBrightness[i] = 0;
        }

        this.img = imagePiece;

        this.colorImgPixData.clear();

        for (int i=0; i<4; i++)
        {
            Bitmap tmpImg = RotateBitmap(this.img, (360 - i*90));
            tmpImg = Bitmap.createScaledBitmap(tmpImg, 5, 5, false);
            this.colorImg[i] = tmpImg;
            this.colorImgPixData.add(this.getPixels(tmpImg));
        }

        PixelData[][] pixelData = this.getPixels(this.img);

        this.processTile(pixelData, this);

        int nPiecesHoriz = cols;
        int nPiecesVert = rows;

        int x = this.id % nPiecesHoriz;
        int y = this.id / nPiecesHoriz;

        double distancex = 0.0;
        if (x < (nPiecesHoriz/2)){
            distancex = (double)x / (double)(nPiecesHoriz/2);  // 0 - 1 to center
        } else {
            distancex = 1 - (double)(x - nPiecesHoriz/2) / (double)(nPiecesHoriz/2);  // 1 - 0 from center
        }

        double distancey = 0.0;
        if (y < (nPiecesVert/2)){
            distancey = (double)y / (double)(nPiecesVert/2);  // 0 - 1 to center
        } else {
            distancey = 1 - (double)(y - nPiecesVert/2) / (double)(nPiecesVert/2);  // 1 - 0 from center
        }

        this.distanceFromCenter = Math.min(distancex, distancey);

        //print("id = %i, x y (%i %i), distance = %f \n", id, x, y, distanceFromCenter);
    }
}
