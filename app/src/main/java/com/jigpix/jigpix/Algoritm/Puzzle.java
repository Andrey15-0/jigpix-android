package com.jigpix.jigpix.Algoritm;

import android.graphics.Bitmap;
import java.util.Random;

/**
 * Created by andrey_sereda on 01.09.2016.
 */
public class Puzzle {
    public PuzzlePiece[] pieces = null;
    private PuzzlePiece[] targetPieces = null;

    public int nPiecesHoriz = 0;
    public int nPiecesVert = 0;
    public int nTotalPieces = 0;
    private int smallResHoriz = 0;
    private int smallResVert = 0;

    public double powerFunc = 0.0;
    public double centralImortance = 0.0;

    private class Rotation{
        int rotated = 0;
    }

    //--------------------------------------------------------------
    public void setup(int cols, int rows)
    {
        centralImortance		= 1;
        powerFunc				= 0.6;
        nPiecesHoriz			= cols;//20;
        nPiecesVert				= rows;//26;
        nTotalPieces			= nPiecesHoriz * nPiecesVert;
        smallResHoriz			= cols * PuzzlePiece.CHOPSIZE; // 300;
        smallResVert			= rows * PuzzlePiece.CHOPSIZE; //390;

        // allocate the pieces
        this.pieces = new PuzzlePiece[nTotalPieces];
        this.targetPieces = new PuzzlePiece[nTotalPieces];

        for (int i = 0; i < nTotalPieces; i++) {
            this.pieces[i] = new PuzzlePiece();
            this.targetPieces[i] = new PuzzlePiece();
        }
    }

    //--------------------------------------------------------------
    public void setFromImage(Bitmap inputImage, Boolean target)
    {
        Bitmap resizedImage = Bitmap.createScaledBitmap(inputImage, smallResHoriz, smallResVert, false);
        int pieceSizeW = smallResHoriz / nPiecesHoriz;

        for (int p = 0; p < nTotalPieces; p++)
        {
            int row = p / nPiecesHoriz;
            int col = p % nPiecesHoriz;

            Bitmap cropImage = Bitmap.createBitmap(resizedImage, col * pieceSizeW,row * pieceSizeW,pieceSizeW, pieceSizeW);

            if(target)
            {
                this.targetPieces[p] = new PuzzlePiece();
                this.targetPieces[p].id = p;
                this.targetPieces[p].calculateStatistics(cropImage, nPiecesHoriz, nPiecesVert);
            }
            else
            {
                this.pieces[p] = new PuzzlePiece();
                this.pieces[p].id = p;
                this.pieces[p].calculateStatistics(cropImage, nPiecesHoriz, nPiecesVert);
            }
        }
    }

    //--------------------------------------------------------------
    public Boolean tryToSwapStuff()
    {
        Random rand = new Random();
        int posA = rand.nextInt(nTotalPieces);
        int posB = rand.nextInt(nTotalPieces);

        if (posA == posB)
        {
            return false;
        }

        double CAatTA = 0.0, CBatTB = 0.0;
        double CAatTB = 0.0, CBatTA = 0.0;

        PuzzlePiece TAptr = targetPieces[ posA ];
        PuzzlePiece TBptr = targetPieces[ posB ];
        PuzzlePiece CAptr = pieces[ posA ];
        PuzzlePiece CBptr = pieces[ posB ];

        Rotation notused = new Rotation();
        /* calc mismatches */
        CAatTA = mismatch(CAptr, TAptr, notused); /* CCWs already recorded  */
        /* will CAatTA *= (C_CENT * TAdis); */

        CBatTB = mismatch(CBptr, TBptr, notused); /* CCWs already recorded  */
        /* will CBatTB *= (C_CENT * TBdis); */

        Rotation AatBrots = new Rotation();
        CAatTB = mismatch(CAptr, TBptr, AatBrots); /* AatB, newCCWs if swapt */
        /* will CAatTB *= (C_CENT * TBdis); */

        Rotation BatArots = new Rotation();
        CBatTA = mismatch(CBptr, TAptr, BatArots); /* BatA, newCCWs if swapt */
        /* will CBatTA += (C_CENT * TAdis); */


        //	/* Central mismatch can be 1 to 9 times as important as at corner.
        //	 * Its adjusted value f(CentralImportance * DistToCorner) is thus
        //	 * about from 100% to 900% of originglly (for max import 1 thru 9),
        //	 * approximated by: newval = oldval + (oldval*(import-1)*distance)/10
        //	 * (Note two different distances are involved for locs A & B)
        //	 */

        double importminus = this.centralImortance;

        int loca = TAptr.id;
        int locb = TBptr.id;

        int COLS = this.nPiecesHoriz;

        int Acol = loca%COLS;
        int Arow = loca/COLS;
        int Bcol = locb%COLS;
        int Brow = locb/COLS;

        int ax =  (Acol<10 ? Acol : 19-Acol);
        int ay =  (Arow<13 ? Arow : 25-Arow);
        int bx =  (Bcol<10 ? Bcol : 19-Bcol);
        int by =  (Brow<13 ? Brow : 25-Brow);

        //        let Adist = (Acol<10 ? Acol : 19-Acol)+(Arow<13 ? Arow : 25-Arow);
        //        let Bdist = (Bcol<10 ? Bcol : 19-Bcol)+(Brow<13 ? Brow : 25-Brow);

        double distanceA = Math.min(Math.pow(Math.sqrt((double)(ax*ax)) / Math.sqrt(10*10), powerFunc) , Math.pow(Math.sqrt((double)(ay*ay)) / Math.sqrt(13*13),powerFunc));
        double distanceB = Math.min(Math.pow(Math.sqrt((double)(bx*bx)) / Math.sqrt (10*10),powerFunc) , Math.pow(Math.sqrt((double)(by*by)) / Math.sqrt(13*13),powerFunc));

        distanceA *= (importminus / 5.0);
        distanceB *= (importminus / 5.0);

        CAatTA += ((CAatTA  * distanceA)) ;
        CBatTB += ((CBatTB  * distanceB)) ;
        CAatTB += ((CAatTB  * distanceB)) ;
        CBatTA += ((CBatTA  * distanceA)) ;


        //print("mismatch %i %i \n", CAatTB + CBatTA, CAatTA + CBatTB);
        /* if chopup tiles best as they are, do not swap */
        if((CAatTB + CBatTA) >= (CAatTA + CBatTB)) {
            //print("we're not swapping \n");
            return false;
        } else{

        }
        /* Exchange 1. plant anticipated new miss data */
        pieces[posA].rotation = AatBrots.rotated;
        pieces[posB].rotation = BatArots.rotated;

        PuzzlePiece temp = pieces[posA];
        pieces[posA] = pieces[posB];
        pieces[posB] = temp;

        return true;
    }

    //--------------------------------------------------------------
    private double mismatch(PuzzlePiece targPtr, PuzzlePiece chopPtr, Rotation bestRot)
    {
        // ------------------------------------------------------------
        // find the best rotation
        // ------------------------------------------------------------

        int Crit = 0, Cbot = 0;  /* chopup tile */
        int Trit = 0, Tbot = 0;  /* target tile */
        double SumRitBotSq = 0.0, nowsum = 0.0;
        int newbot = 0;

//        Cbri = chopPtr.bri;
        Crit = chopPtr.rit;
        Cbot = chopPtr.bot;
//        Tbri = targPtr.bri;
        Trit = targPtr.rit;
        Tbot = targPtr.bot;

        SumRitBotSq = 999999999;   /* <-- will certainly be lowered */
        for (int i = 0; i < 4; i++) {
            double tmp = (double)((Crit-Trit)*(Crit-Trit) + (Cbot-Tbot)*(Cbot-Tbot));
            nowsum = Math.sqrt(tmp);
            //cout << nowsum << endl;
            if(SumRitBotSq > nowsum){
                SumRitBotSq = nowsum;
                bestRot.rotated = i;  /* best addt'l CCWs IFF swapped */
            }
            /* rotate chopup tile vals 90 degrees (CCW TEMP VALS ONLY) */
            newbot = -Crit;
            Crit   =  Cbot;
            Cbot   =  newbot;
        }


        // ------------------------------------------------------------
        // w/ the best rotation, find the per pixel difference of the smaller images
        // based on this metric http://www.compuphase.com/cmetric.htm
        // ------------------------------------------------------------

        double distance = 0.0;

        PuzzlePiece.PixelData[][] pixelsA = chopPtr.colorImgPixData.get(bestRot.rotated);
        PuzzlePiece.PixelData[][] pixelsB = targPtr.colorImgPixData.get(0);

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                int rmean = (pixelsA[i][j].r + pixelsB[i][j].r) / 2;
                int r = pixelsA[i][j].r - pixelsB[i][j].r;
                int g = pixelsA[i][j].g - pixelsB[i][j].g;
                int b = pixelsA[i][j].b - pixelsB[i][j].b;
                double tmp = (double)((((512+rmean)*r*r)>>8) + 4*g*g + (((767-rmean)*b*b)>>8));
                distance += Math.sqrt(tmp);
            }
        }
        distance /= 25.0;

        return(distance*distance);

    }
}
