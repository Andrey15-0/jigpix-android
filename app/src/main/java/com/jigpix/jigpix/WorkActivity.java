package com.jigpix.jigpix;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.jigpix.jigpix.Algoritm.Puzzle;
import com.jigpix.jigpix.Algoritm.PuzzlePiece;
import com.jigpix.jigpix.Utils.ActionSheet;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by andrey_sereda on 13.01.2016.
 */
public class WorkActivity extends AppCompatActivity {

    private static final int RESULT_LOAD_IMAGE = 100;
    private static final int REQUEST_IMAGE_CAPTURE = 101;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 200;
    private static final int PERMISSION_REQUEST_CODE = 300;

    private int mSourceImage = -1;

    private Bitmap mTargetImage = null;

    private AsyncJigpixTask mAsyncJigpixTask = null;

    public Pieces puzzlePieces = Pieces.Medium520;

    private AspectRatioImageView mImageViewProcess = null;

    private ImageButton mLeftImageButton = null;
    private ImageButton mRightImageButton = null;

    private ImageButton mImageButtonShare = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize the SDK before executing any other operations,
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_work);

        mImageViewProcess = (AspectRatioImageView) findViewById(R.id.imageViewProcess);
        mImageViewProcess.setAspectRatioEnabled(true);

        mLeftImageButton = (ImageButton) findViewById(R.id.imageButtonLeft);
        mLeftImageButton.setVisibility(View.GONE);
        mRightImageButton = (ImageButton) findViewById(R.id.imageButtonRight);
        mRightImageButton.setVisibility(View.GONE);

        int choose = getIntent().getIntExtra("choose", -1);
        int piecesImage = R.drawable.icon_520;
        if (choose == 1) {
            mSourceImage = R.drawable.embarrassing_politicians;
            piecesImage = R.drawable.icon_300;
            puzzlePieces = Pieces.Small300;
            mImageViewProcess.setAspectRatio(3.0f / 4.0f);
        } else if (choose == 2) {
            mSourceImage = R.drawable.cute_animals;
            mImageViewProcess.setAspectRatio(17.0f / 22.0f);
        } else if (choose == 3) {
            mSourceImage = R.drawable.venus_birth;
            mImageViewProcess.setAspectRatio(17.0f / 22.0f);
        }

        Button buttonConvert = (Button) findViewById(R.id.buttonConvert);
        buttonConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button buttonConvert = (Button) findViewById(R.id.buttonConvert);
                String convertText = buttonConvert.getText().toString();

                if (convertText.equals("Convert")) {
                    convertText = "Converting...";
                    Bitmap sourceImage = BitmapFactory.decodeResource(getResources(), mSourceImage);
                    Bitmap targetImage = ((BitmapDrawable) mImageViewProcess.getDrawable()).getBitmap();
                    mTargetImage = targetImage;
                    mAsyncJigpixTask = new AsyncJigpixTask();
                    mAsyncJigpixTask.execute(sourceImage, targetImage);
                    //mJigpixAlgoritm.Start(sourceImage, targetImage);
                    //imageViewProcess.setImageBitmap(mJigpixAlgoritm.GetOutput());
                    mLeftImageButton.setVisibility(View.VISIBLE);
                    mLeftImageButton.setImageBitmap(mTargetImage);
                } else if (convertText.equals("Converting...")) {
                    convertText = "Get Code";
                } else if (convertText.equals("Get Code")) {
                    convertText = "DONE";
                    mRightImageButton.setVisibility(View.VISIBLE);
                    mRightImageButton.setImageBitmap(((BitmapDrawable) mImageViewProcess.getDrawable()).getBitmap());
                    mImageViewProcess.setImageBitmap(mAsyncJigpixTask.codeImage);
                } else if (convertText.equals("DONE")) {
                    convertText = "Convert";
                    mImageViewProcess.setImageBitmap(mTargetImage);
                    mLeftImageButton.setVisibility(View.GONE);
                    mRightImageButton.setVisibility(View.GONE);
                }

                buttonConvert.setText(convertText);
            }
        });

        ImageButton imageButtonBack = (ImageButton) findViewById(R.id.imageButtonBack);
        Bitmap myImg = BitmapFactory.decodeResource(getResources(), mSourceImage);
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap rotated = Bitmap.createBitmap(myImg, 0, 0, myImg.getWidth(), myImg.getHeight(), matrix, true);
        imageButtonBack.setBackground(new BitmapDrawable(getResources(), rotated));
        imageButtonBack.setImageResource(piecesImage);
        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageButton imageButtonInfo = (ImageButton) findViewById(R.id.imageButtonInfo);
        imageButtonInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),
//                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus libero tellus, varius blandit erat ac, sagittis iaculis augue. Nunc commodo finibus faucibus. Nullam magna tellus, tincidunt vel erat ac, vulputate pretium neque. Proin ullamcorper pellentesque ipsum, sit amet tincidunt nunc. Suspendisse fermentum arcu a ante faucibus pulvinar. Vestibulum at urna eget magna iaculis ullamcorper vitae et lacus. Nam eleifend ante tortor, eget cursus ex aliquam vitae. Sed non commodo enim, ac scelerisque nibh. Quisque laoreet magna a orci sodales, varius maximus ex posuere. Aliquam id quam efficitur, auctor elit a, tempor nisl. Nullam ultricies volutpat ornare. Maecenas aliquet leo quis tempus ultricies. Duis non pharetra ante.",
//                        Toast.LENGTH_LONG).show();

                ActionSheet.createBuilder(getApplicationContext(), getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus libero tellus, varius blandit erat ac, sagittis iaculis augue. Nunc commodo finibus faucibus. Nullam magna tellus, tincidunt vel erat ac, vulputate pretium neque. Proin ullamcorper pellentesque ipsum, sit amet tincidunt nunc. Suspendisse fermentum arcu a ante faucibus pulvinar. Vestibulum at urna eget magna iaculis ullamcorper vitae et lacus. Nam eleifend ante tortor, eget cursus ex aliquam vitae. Sed non commodo enim, ac scelerisque nibh. Quisque laoreet magna a orci sodales, varius maximus ex posuere. Aliquam id quam efficitur, auctor elit a, tempor nisl. Nullam ultricies volutpat ornare. Maecenas aliquet leo quis tempus ultricies. Duis non pharetra ante.")
                        .setCancelableOnTouchOutside(true)
                        .show();
            }
        });

        mImageButtonShare = (ImageButton) findViewById(R.id.imageButtonShare);
        mImageButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionSheet.createBuilder(getApplicationContext(), getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Photos", "Message", "Mail", "Facebook")
                        .setOtherButtonIcons(
                                R.drawable.photos,
                                R.drawable.messages,
                                R.drawable.mail,
                                R.drawable.facebook)
                        .setCancelableOnTouchOutside(true)
                        .setListener(new ActionSheet.ActionSheetListener() {
                            @Override
                            public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
                            }

                            @Override
                            public void onOtherButtonClick(ActionSheet actionSheet, int index) {
                                if (index == 0) {
                                    Bitmap imageForSave = createImageForSaving();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                                    String currentDateAndTime = sdf.format(new Date());
                                    String title = String.format("Jigpix %1$s", currentDateAndTime);
                                    String description = "From Jigpix";
                                    String imagePath = MediaStore.Images.Media.insertImage(getContentResolver(), imageForSave, title, description);
                                    Uri uri = Uri.parse(imagePath);
                                    Toast.makeText(getApplicationContext(), "Image Saved Successfully", Toast.LENGTH_SHORT).show();
                                } else if (index == 1) {
                                    Bitmap imageForSave = createImageForSaving();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                                    String currentDateAndTime = sdf.format(new Date());
                                    String title = String.format("Jigpix %1$s", currentDateAndTime);
                                    String description = "From Jigpix";
                                    String imagePath = MediaStore.Images.Media.insertImage(getContentResolver(), imageForSave, title, description);
                                    Uri uri = Uri.parse(imagePath);

                                    Intent mmsIntent = new Intent(Intent.ACTION_SEND);
                                    mmsIntent.putExtra("sms_body", "Send from Jigpix");
                                    mmsIntent.putExtra(Intent.EXTRA_STREAM, uri);
                                    mmsIntent.setType("image/gif");
                                    startActivity(mmsIntent); //Intent.createChooser(mmsIntent,"Send"));
                                } else if (index == 2) {
                                    Bitmap imageForSave = createImageForSaving();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                                    String currentDateAndTime = sdf.format(new Date());
                                    String title = String.format("Jigpix %1$s", currentDateAndTime);
                                    String description = "From Jigpix";
                                    String imagePath = MediaStore.Images.Media.insertImage(getContentResolver(), imageForSave, title, description);
                                    Uri uri = Uri.parse(imagePath);

                                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                    emailIntent.setType("text/plain");
                                    //emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"jon@example.com"}); // recipients
                                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Sending you e-mail from Jigpix");
                                    emailIntent.putExtra(Intent.EXTRA_TEXT, "Your message here...");
                                    emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
                                    startActivity(emailIntent);
                                } else if (index == 3) {
                                    //Toast.makeText(getApplicationContext(), "Functionality not implemented!", Toast.LENGTH_SHORT).show();
                                    if (ShareDialog.canShow(SharePhotoContent.class)) {
                                        Bitmap imageForSave = createImageForSaving();
                                        SharePhoto photo = new SharePhoto.Builder()
                                                .setBitmap(imageForSave)
                                                .setCaption("Send from Jigpix")
                                                .build();
                                        SharePhotoContent content = new SharePhotoContent.Builder()
                                                .addPhoto(photo)
                                                .build();
//                                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
//                                                .setContentTitle("Hello Facebook")
//                                                .setContentDescription(
//                                                        "The 'Hello Facebook' sample  showcases simple Facebook integration")
//                                                .s.setContentUrl(Uri.parse("http://developers.facebook.com/android"))
//                                                .build();

                                        ShareDialog.show(WorkActivity.this, content);
                                    }
                                }
                            }
                        }).show();
            }
        });

        ImageButton imageButtonCamera = (ImageButton) findViewById(R.id.imageButtonCamera);
        imageButtonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionSheet.createBuilder(getApplicationContext(), getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Library", "Photo", "Example")
                        .setOtherButtonIcons(
                                R.drawable.photos,
                                R.drawable.camera,
                                R.drawable.example)
                        .setCancelableOnTouchOutside(true)
                        .setListener(new ActionSheet.ActionSheetListener() {
                            @Override
                            public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
                            }

                            @Override
                            public void onOtherButtonClick(ActionSheet actionSheet, int index) {
                                if (index == 0) {
                                    startGalleryActivity();
                                } else if (index == 1) {
                                    if (checkCallingOrSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
                                        }
                                    } else {
                                        startCameraActivity();
                                    }
                                } else if (index == 2) {
                                    Bitmap sampleImage = BitmapFactory.decodeResource(getResources(), R.drawable.mona_lisa);
                                    mImageViewProcess.setImageBitmap(sampleImage);
                                }
                            }
                        }).show();
            }
        });

        if (Build.VERSION.SDK_INT >= 23) {
            if (!checkPermission()) {
                requestPermission();
            }
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(WorkActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(WorkActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(WorkActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
            mImageButtonShare.setEnabled(false);
        } else {
            ActivityCompat.requestPermissions(WorkActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    private void startCameraActivity() {
//        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        takePhotoIntent.putExtra("return-data", true);
//        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(ImagePicker.getTempFile(getApplicationContext())));
//        startActivityForResult(takePhotoIntent, RESULT_LOAD_IMAGE);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void startGalleryActivity() {
        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickIntent, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_LOAD_IMAGE:
                Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                if (bitmap != null) {
                    mImageViewProcess.setImageBitmap(bitmap);
                }
                break;
            case REQUEST_IMAGE_CAPTURE:
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                imageBitmap = PuzzlePiece.RotateBitmap(imageBitmap, 90);
                mImageViewProcess.setImageBitmap(imageBitmap);
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera
                this.startCameraActivity();
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        } else if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Log.e("value", "Permission Denied, You cannot use local drive .");
                mImageButtonShare.setEnabled(false);
            }
        }
    }

    private Bitmap createImageForSaving() {
        Bitmap resultBitmap = null;

        Button buttonConvert = (Button) findViewById(R.id.buttonConvert);
        String convertText = buttonConvert.getText().toString();

        if (convertText.equals("DONE")) {
            Bitmap logoBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.jigpix_logo);
            Bitmap puzzleBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.puzzle);
            Bitmap selfieBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.mona_lisa);
            Bitmap result2Bitmap = ((BitmapDrawable) mRightImageButton.getDrawable()).getBitmap();
            Bitmap codeBitmap = ((BitmapDrawable) mImageViewProcess.getDrawable()).getBitmap();

            Bitmap newBitmap = Bitmap.createBitmap(1120, 1408, codeBitmap.getConfig());
            Canvas canvas = new Canvas(newBitmap);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            canvas.drawRect(0, 0, newBitmap.getWidth(), newBitmap.getHeight(), paint);
            paint.setColor(Color.RED);
            paint.setStrokeWidth(3);
            paint.setStyle(Paint.Style.STROKE);

            int heightOffset = newBitmap.getHeight() / 5;

            int border = 40;
            int yOffset = 5;

            //Draw logo
            logoBitmap = Bitmap.createScaledBitmap(logoBitmap, (int) (newBitmap.getWidth() / 2.5) - border, heightOffset - yOffset - 30, false);
            canvas.drawBitmap(logoBitmap, border, yOffset + border + 10, paint);

            int smallImageWidth = (newBitmap.getWidth() - (int) (newBitmap.getWidth() / 2.5)) / 3 - 40;

            //Draw puzzle
            puzzleBitmap = Bitmap.createScaledBitmap(puzzleBitmap, smallImageWidth, heightOffset - yOffset, false);
            canvas.drawBitmap(puzzleBitmap, (int) (newBitmap.getWidth() / 2.5), yOffset + border, paint);

            //Draw plus
            canvas.drawLine(
                    (int) (newBitmap.getWidth() / 2.5) + smallImageWidth + 2,
                    (int) (heightOffset / 2.0 + border),
                    (int) (newBitmap.getWidth() / 2.5) + smallImageWidth + 2 + 20,
                    (int) (heightOffset / 2.0 + border),
                    paint);
            canvas.drawLine(
                    (int) (newBitmap.getWidth() / 2.5) + smallImageWidth + 2 + 10,
                    (int) (heightOffset / 2.0 + border - 10),
                    (int) (newBitmap.getWidth() / 2.5) + smallImageWidth + 2 + 10,
                    (int) (heightOffset / 2.0 + border + 10),
                    paint);

            //Draw selfie
            selfieBitmap = Bitmap.createScaledBitmap(selfieBitmap, smallImageWidth, heightOffset - yOffset, false);
            canvas.drawBitmap(selfieBitmap, (int) (newBitmap.getWidth() / 2.5) + smallImageWidth + 2 + 20 + 2, yOffset + border, paint);

            //Draw equal
            canvas.drawLine(
                    (int) (newBitmap.getWidth() / 2.5 + smallImageWidth + 2 + 20 + 2 + smallImageWidth + 2),
                    heightOffset / 2 - 8 + border,
                    (int) (newBitmap.getWidth() / 2.5 + smallImageWidth + 2 + 20 + 2 + smallImageWidth + 2 + 20),
                    heightOffset / 2 - 8 + border,
                    paint);
            canvas.drawLine(
                    (int) (newBitmap.getWidth() / 2.5 + smallImageWidth + 2 + 20 + 2 + smallImageWidth + 2),
                    heightOffset / 2 + 4 + border,
                    (int) (newBitmap.getWidth() / 2.5 + smallImageWidth + 2 + 20 + 2 + smallImageWidth + 2 + 20),
                    heightOffset / 2 + 4 + border,
                    paint);

            //Draw result
            result2Bitmap = Bitmap.createScaledBitmap(result2Bitmap, smallImageWidth, heightOffset - yOffset, false);
            canvas.drawBitmap(result2Bitmap, (int) (newBitmap.getWidth() / 2.5) + smallImageWidth + 2 + 20 + 2 + smallImageWidth + 2 + 20, yOffset + border, paint);

            //Draw code
            codeBitmap = Bitmap.createScaledBitmap(codeBitmap, newBitmap.getWidth() - 2 * border, newBitmap.getHeight() - heightOffset - 2 * border - yOffset, false);
            canvas.drawBitmap(codeBitmap, border, heightOffset + border + yOffset, paint);

            resultBitmap = newBitmap;
        } else {
            Bitmap imageBitmap = ((BitmapDrawable) mImageViewProcess.getDrawable()).getBitmap();

            resultBitmap = Bitmap.createScaledBitmap(imageBitmap, 1120, 1408, false);
        }

        return resultBitmap;
    }

    private enum Pieces {
        Small300,
        Medium520
    }

    private class AsyncJigpixTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {
        @Override
        protected Bitmap doInBackground(Bitmap... bitmaps) {
            this.Start(bitmaps[0], bitmaps[1]);

            return outputImage;
        }

        @Override
        protected void onProgressUpdate(Bitmap... values) {
            super.onProgressUpdate(values);

            AspectRatioImageView imageViewProcess = (AspectRatioImageView) findViewById(R.id.imageViewProcess);
            imageViewProcess.setImageBitmap(outputImage);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            AspectRatioImageView imageViewProcess = (AspectRatioImageView) findViewById(R.id.imageViewProcess);
            imageViewProcess.setImageBitmap(outputImage);

            Button buttonConvert = (Button) findViewById(R.id.buttonConvert);
            buttonConvert.callOnClick();
        }

        private Puzzle puzzle = new Puzzle();

        private Bitmap outputImage = null;
        private Bitmap codeImage = null;

        //MARK: - Consts
        int COLS = puzzlePieces == Pieces.Small300 ? 15 : 20;
        int ROWS = puzzlePieces == Pieces.Small300 ? 20 : 26;

        //MARK: - Start
        public void Start(Bitmap source, Bitmap target) {
            this.outputImage = source;

            publishProgress(outputImage);

            COLS = puzzlePieces == Pieces.Small300 ? 15 : 20;
            ROWS = puzzlePieces == Pieces.Small300 ? 20 : 26;

            this.puzzle.setup(COLS, ROWS);

            this.puzzle.setFromImage(source, false);
            this.puzzle.setFromImage(target, true);

            this.puzzle.centralImortance = 60.0;
            this.puzzle.powerFunc = 3.0;

            Boolean swapped = false;
            int refreshCnt = 10000;
            int cnt = 0;
            for (int i = 0; i < 1300000; i++) {
                if (isCancelled()) {
                    break;
                }

                if (this.puzzle.tryToSwapStuff()) {
                    swapped = true;
                }

                cnt = cnt + 1;
                if (cnt >= refreshCnt) {
                    this.changeImage();
                    cnt = 0;
                    if (!swapped) {
                        break;
                    }
                    swapped = false;
                    //print(i);
                }
            }

            //SAVE OBJECT TO OUTPUT IMAGE
            this.saveOutputImage(true);

            //GENERATE CODE IMAGES PICTURE
            this.generateCodeImage();
        }

        //MARK: - Fire image changed event with saving
        public void changeImage() {
            this.saveOutputImage(false);

            publishProgress(outputImage);
        }

        //MARK: - Saves canvas to output image
        public void saveOutputImage(Boolean last) {
            int COLS = this.puzzle.nPiecesHoriz;
            int ROWS = this.puzzle.nPiecesVert;
            int centers = (int) (this.puzzle.pieces[0].img.getWidth()) * (last ? 2 : 1);

            Bitmap newBitmap = Bitmap.createBitmap(centers * COLS, centers * ROWS, this.outputImage.getConfig());
            Canvas canvas = new Canvas(newBitmap);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            canvas.drawRect(0, 0, centers * COLS, centers * ROWS, paint);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(2);
            paint.setStyle(Paint.Style.STROKE);

            for (int p = 0; p < this.puzzle.nTotalPieces; p++) {
                int row = p / COLS;
                int col = p % COLS;
                Bitmap pieceImage = this.puzzle.pieces[p].img;
                float degrees = this.puzzle.pieces[p].rotation * 90;
                if (degrees > 0) {
                    pieceImage = PuzzlePiece.RotateBitmap(pieceImage, degrees);
                }
                pieceImage = Bitmap.createScaledBitmap(pieceImage, centers, centers, false);
                canvas.drawBitmap(pieceImage, col * centers, row * centers, paint);
                canvas.drawRect(col * centers, row * centers, centers * COLS, centers * ROWS, paint);
            }

            this.outputImage = newBitmap;
        }

        //MARK: - Generate codes image
        private void generateCodeImage() {
            this.codeImage = null;

            Bitmap cropImage = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.jigpix300);
            if(puzzlePieces == Pieces.Medium520)
                cropImage = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.jigpix520);

            int centers = cropImage.getWidth() / COLS;//this.puzzle.pieces[0].img.getWidth() * 2;

            Bitmap newBitmap = Bitmap.createBitmap(centers * COLS, centers * ROWS, this.outputImage.getConfig());
            Canvas canvas = new Canvas(newBitmap);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            canvas.drawRect(0, 0, centers * COLS, centers * ROWS, paint);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(2);
            paint.setStyle(Paint.Style.STROKE);

            for (int p = 0; p < this.puzzle.nTotalPieces; p++) {
                int row = p / this.puzzle.nPiecesHoriz;
                int col = p % this.puzzle.nPiecesHoriz;
                int pieceNo = this.puzzle.pieces[p].id;
                Bitmap pieceImage = Bitmap.createBitmap(cropImage, pieceNo % this.puzzle.nPiecesHoriz * centers, pieceNo / this.puzzle.nPiecesHoriz * centers, centers, centers);
                float degrees = this.puzzle.pieces[p].rotation * 90;
                if (degrees > 0) {
                    pieceImage = PuzzlePiece.RotateBitmap(pieceImage, degrees);
                }
                pieceImage = Bitmap.createScaledBitmap(pieceImage, centers, centers, false);
                canvas.drawBitmap(pieceImage, col * centers, row * centers, paint);
                canvas.drawRect(col * centers, row * centers, centers * COLS, centers * ROWS, paint);
            }

            this.codeImage = newBitmap;
        }
    }
}
