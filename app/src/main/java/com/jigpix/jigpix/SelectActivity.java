package com.jigpix.jigpix;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

/**
 * Created by andrey_sereda on 11.01.2016.
 */
public class SelectActivity extends AppCompatActivity {

    FeatureCoverFlow mCoverFlow = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        mCoverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        ArrayList<Integer> sourceList = new ArrayList<Integer>();
        sourceList.add(R.drawable.first);
        sourceList.add(R.drawable.second);
        sourceList.add(R.drawable.third);
        CovertAdapter coverAdapter = new CovertAdapter(this, R.layout.cover_image_item, sourceList);
        mCoverFlow.setAdapter(coverAdapter);

        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //TODO CoverFlow item clicked
                Intent workIntent = new Intent(getApplicationContext(), WorkActivity.class);
                workIntent.putExtra("choose", position + 1);
                startActivity(workIntent);
            }
        });

        mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                //TODO CoverFlow stopped to position
            }

            @Override
            public void onScrolling() {
                //TODO CoverFlow began scrolling
            }
        });
    }
}

class CovertAdapter extends ArrayAdapter<Integer> {
    private int mResource;
    protected LayoutInflater mInflater;

    public CovertAdapter(Context ctx, int resourceId, List<Integer> objects) {
        super(ctx, resourceId, objects);
        mResource = resourceId;
        mInflater = LayoutInflater.from(ctx);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = (RelativeLayout) mInflater.inflate(mResource, null);
        int imageResource = getItem(position);
        ImageView coverImageView = (ImageView) convertView.findViewById(R.id.cover_image_view);
        coverImageView.setImageResource(imageResource);

        return convertView;
    }
}
