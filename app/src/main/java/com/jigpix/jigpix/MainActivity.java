package com.jigpix.jigpix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        //the whole screen becomes sensitive to touch
        RelativeLayout relativeLayoutMain = (RelativeLayout) findViewById(R.id.relativeLayoutMain);
        relativeLayoutMain.setOnTouchListener(this);

        this.animate();
    }

    private void animate() {
        final TextView textViewTapSomewhere = (TextView)findViewById(R.id.textViewTapSomewhere);

        final AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);
        animation1.setDuration(1000);
        //animation1.setStartOffset(1000);
        final AlphaAnimation animation2 = new AlphaAnimation(1.0f, 0.0f);
        animation2.setDuration(1000);
        //animation2.setStartOffset(1000);
        animation1.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation2 when animation1 ends (continue)
                textViewTapSomewhere.startAnimation(animation2);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

        });
        //animation2 AnimationListener
        animation2.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation1 when animation2 ends (repeat)
                textViewTapSomewhere.startAnimation(animation1);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

        });

        textViewTapSomewhere.startAnimation(animation1);
    }

    public boolean onTouch(View v, MotionEvent event){
        Intent selectIntent = new Intent(getApplicationContext(), SelectActivity.class);
        startActivity(selectIntent);
        //overridePendingTransition(R.anim.activity_open_enter,R.anim.end);

        return false;//false indicates the event is not consumed
    }
}
